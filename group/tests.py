from django.contrib.auth import get_user_model
from django.test import TestCase
from django.test import Client
from django.urls import reverse

from group.models import Group


class GroupTestCase(TestCase):
    def setUp(self):
        User = get_user_model()
        self.user = User.objects.create_user(username='silly', email='silly@a.com', password='silly')
        self.client = Client()
        logged_in = self.client.login(username='silly', email='silly@a.com', password='silly')
        self.assertEqual(logged_in, True)

    def test_create_group_wrong_method(self):
        url = reverse('group:create')
        res = self.client.get(url)
        self.assertRedirects(res, '/')

    def test_create_group(self):
        url = reverse('group:create')
        res = self.client.post(url, {'name': 'silly', 'description': 'silly'})
        self.assertRedirects(res, '/group/?group=1')

        gps = Group.objects.all()
        self.assertEqual(len(gps), 1)

    def test_index_group_worng(self):
        url = reverse('group:index')
        res = self.client.get(url)

        self.assertRedirects(res, '/')

    def test_index_group(self):
        Group.objects.create(name='ok', creator=self.user)

        url = reverse('group:index')
        url += '/?group=1'
        res = self.client.get(url)
        self.assertTemplateUsed(res, 'group/group.html')

    def test_index_group_add_member_worng(self):
        url = reverse('group:add_member')
        res = self.client.get(url)
        self.assertRedirects(res, '/')

    def test_index_group_settleup_worng(self):
        url = reverse('group:settle_up')
        res = self.client.get(url)
        self.assertRedirects(res, '/')

    def test_index_group_update_worng(self):
        url = reverse('group:update')
        res = self.client.get(url)
        self.assertRedirects(res, '/')

    def test_index_group_read_note_worng(self):
        url = reverse('group:read_note')
        Group.objects.create(name='ok', creator=self.user)
        url += '/?group_id=1;user_id=1'
        res = self.client.get(url)
