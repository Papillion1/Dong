from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, JsonResponse
from django.shortcuts import render, reverse
from .models import *
from django.shortcuts import get_object_or_404
from datetime import datetime
from django.core import serializers


@login_required
def create(request):
    if request.method == 'POST':
        group = Group.objects.create(
            name=request.POST['name'],
            description=request.POST['description'],
            creator=request.user
        )

        GroupMember.objects.create(gp=group, member=request.user)

        members_id = [int(member) for member in request.POST.getlist('friends')]
        members = User.objects.filter(pk__in=members_id)
        for member in members:
            GroupMember.objects.create(gp=group, member=member)

        url = reverse('group:index')
        url += '?group=' + str(group.pk)
        return HttpResponseRedirect(url)

    return HttpResponseRedirect(reverse('homepage'))




@login_required
def index(request):
    if 'group' not in request.GET:
        return HttpResponseRedirect(reverse('homepage'))

    group = get_object_or_404(Group, pk=request.GET['group'])

    members = [{'user': member.member, 'counting': group.counting(member.member)}
               for member in GroupMember.objects.filter(gp=group).all()]

    dongs = [{'dong': g_dong.dong, 'counting': g_dong.dong.counting(request.user)}
             for g_dong in GroupDong.objects.filter(gp=group).order_by('-dong__creation_date').all()]

    friends = [friend for friend in request.user.get_friends()
               if friend.id not in [int(member['user'].id) for member in members]]

    group_notes = GroupNote.objects.filter(gp=group).order_by('-creation_date').all()
    read_notes = [el.note for el in ReadNote.objects.filter(note__in=group_notes, member=request.user).all()]
    new_notes = [n for n in group_notes if n not in read_notes]
    new_note_num = len(new_notes)

    return render(request, 'group/group.html', {
        'group': group,
        'members': members,
        'dongs': dongs,
        'friends': friends,
        'notes': {'new': new_notes, 'old': read_notes},
        'new_note_num': new_note_num,
    })

@login_required
def get_members(request):
    group = get_object_or_404(Group, pk=request.GET['group'])

    members = [{member.member.username: member.member.id}
               for member in GroupMember.objects.filter(gp=group).all()]
    res = {}
    for m in members:
        res.update(m)

    return JsonResponse(res)


@login_required
def add_member(request):
    if request.method == 'POST':
        group = get_object_or_404(Group, pk=request.POST['group_id'])

        members_id = [int(member) for member in request.POST.getlist('friends')]
        members = User.objects.filter(pk__in=members_id)
        for member in members:
            GroupMember.objects.create(gp=group, member=member)

        url = reverse('group:index')
        url += '?group=' + str(request.POST['group_id'])
        return HttpResponseRedirect(url)

    return HttpResponseRedirect(reverse('homepage'))


@login_required
def settle_up_group(request):
    if request.method == 'POST':
        group = get_object_or_404(Group, pk=request.POST['group_id'])

        group_dongs = GroupDong.objects.filter(gp=group).values('dong')
        group_records = Record.objects.filter(dong__in=group_dongs)

        this_user_record = group_records.filter(models.Q(creditor=request.user) | models.Q(debtor=request.user))\
            .filter(is_paid=False)
        amount = 0
        for record in this_user_record:
            record.is_paid = True
            record.pay_date = datetime.now()
            record.save()
            amount += record.amount if record.creditor == request.user else record.amount * -1

        if len(this_user_record):
            for g_member in GroupMember.objects.filter(gp=group):
                Notification.objects.create(
                    person=g_member.member,
                    type=NotificationType.settle_up.value,
                    amount=amount,
                    notifier=request.user
                )

        url = reverse('group:index')
        url += '?group=' + str(request.POST['group_id'])
        return HttpResponseRedirect(url)

    return HttpResponseRedirect(reverse('homepage'))


@login_required
def update(request):
    if request.method == 'POST':
        group = get_object_or_404(Group, pk=request.POST['group_id'])

        group.description = request.POST['description']
        group.save()

        url = reverse('group:index')
        url += '?group=' + str(request.POST['group_id'])
        return HttpResponseRedirect(url)
    return HttpResponseRedirect(reverse('homepage'))


# @login_required
def read_note(request):
    group_notes = GroupNote.objects.filter(gp_id=request.GET['group_id']).all()

    read_notes = [el.note for el in
                  ReadNote.objects.filter(models.Q(note__in=group_notes) & models.Q(member_id=request.GET['user_id'])).all()]

    for note in [n for n in group_notes if n not in read_notes]:
        ReadNote.objects.create(note=note, member_id=request.GET['user_id'])

    return JsonResponse({'success': True})


@login_required
def new_note(request):
    if request.method == 'POST':
        group = get_object_or_404(Group, pk=request.POST['group_id'])

        GroupNote.objects.create(gp=group, creator=request.user, note=request.POST['content'])

        url = reverse('group:index')
        url += '?group=' + str(request.POST['group_id'])
        return HttpResponseRedirect(url)
    return HttpResponseRedirect(reverse('homepage'))
