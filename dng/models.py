from django.core.validators import MinValueValidator
from UserProfile.models import *
import enum


class SplitMethod(enum.Enum):
    equally = 'EQUALLY'
    customize = 'CUSTOMIZE'


class Dong(models.Model):
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    split_method = models.TextField(default=SplitMethod.equally.value, blank=False)
    attachment = models.FileField(null=True)  # TODO may need upload_to
    creation_date = models.DateTimeField(auto_now=True)
    name = models.TextField(blank=False)

    def counting(self, user):
        records = Record.objects.filter(dong=self.id)

        lent = [int(a.amount) for a in records.filter(creditor=user.id).all()]
        owe = [int(a.amount) for a in records.filter(debtor=user.id).all()]

        total = sum(lent) - sum(owe)

        return total

    def get_owes(self, user):
        records = Record.objects.filter(dong=self.id)

        owe = [int(a.amount) for a in records.filter(debtor=user.id).all()]

        return sum(owe)

    def get_pays(self, user):
        records = Record.objects.filter(dong=self.id)

        lent = [int(a.amount) for a in records.filter(creditor=user.id).all()]

        return sum(lent)

    def get_date(self):
        return {
            'year': self.creation_date.strftime('%G'),
            'month': self.creation_date.strftime('%b'),
            'day': self.creation_date.strftime('%e'),
        }


class Record(models.Model):
    dong = models.ForeignKey(Dong, on_delete=models.CASCADE)
    creditor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='creditors')
    debtor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='debtors')
    amount = models.IntegerField(validators=[MinValueValidator(0)], null=False)
    is_paid = models.BooleanField(default=False, null=False)
    pay_date = models.DateTimeField()
    creation_date = models.DateTimeField(auto_now=True)


class DongComment(models.Model):
    dong = models.ForeignKey(Dong, on_delete=models.CASCADE)
    creator = models.ForeignKey(User, on_delete=models.CASCADE)
    note = models.TextField(blank=False)
    creation_date = models.DateTimeField(auto_now=True)
    attachment = models.FileField()


class NotificationType(enum.Enum):
    settle_up = 'SETTLE_UP'
    dong = 'DONG'


class Notification(models.Model):
    person = models.ForeignKey(User, on_delete=models.CASCADE, related_name='person')
    creation_date = models.DateTimeField(auto_now=True)
    type = models.TextField(default=NotificationType.dong.value, blank=False)
    seen = models.BooleanField(default=False)
    dong = models.ForeignKey(Dong, on_delete=models.CASCADE, null=True)
    amount = models.IntegerField(validators=[MinValueValidator(0)], null=False)
    notifier = models.ForeignKey(User, on_delete=models.CASCADE, related_name='notifier')
