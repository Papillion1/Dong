from django.conf.urls import url
from django.contrib import admin
from django.urls import include
from django.urls import path
from dng import views

app_name = 'dng'
urlpatterns = [
    url('create', views.create, name='create'),
    url('seen_notification', views.seen_notification, name='seen_notification'),

    url('', views.index, name='index')
]
