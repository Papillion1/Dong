from django.contrib.auth.decorators import login_required
from django.shortcuts import render


@login_required
def profile(request):
    if request.method == 'GET':
        return render(request, 'account/profile.html')

    user = request.user
    user.profile_picture = request.FILES['file']
    user.save()
    return render(request, 'account/profile.html')


