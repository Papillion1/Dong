from allauth.account.forms import SignupForm


class MyCustomSignupForm(SignupForm):

    def clean(self):
        super().clean()
        username = self.cleaned_data.get('username')
        if len(username) > 15:
            self.add_error("username", "Username can not be longer that 15 characters.")

        return self.cleaned_data

    def save(self, request):

        # Ensure you call the parent classes save.
        # .save() returns a User object.
        user = super(MyCustomSignupForm, self).save(request)

        # Add your own processing here.

        # You must return the original result.
        return user