from django.conf.urls import url
from django.urls import path

from UserProfile import views

app_name = 'UserProfile'
urlpatterns = [
    url('getusers/', views.get_users, name='getusers'),
    url('add_friend/', views.addfriend, name='addfriend'),
    path('friend/<int:id>/', views.get_friend_detail, name='get_friend_detail'),
    path('settleup/', views.settle_up, name='settle_up'),
]
