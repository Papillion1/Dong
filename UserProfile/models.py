from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models import Q

import group
import dng


class User(AbstractUser):
    # Add fields for your user here
    # silly_field_for_test = models.CharField(max_length=30, blank=True)
    profile_picture = models.ImageField(null=True)

    def get_friends(self):
        ids = [friend.friend1.id for friend in
               Relation.objects.filter(models.Q(friend2=self)).all()]
        ids += [friend.friend2.id for friend in
                Relation.objects.filter(models.Q(friend1=self)).all()]
        return User.objects.filter(pk__in=ids).all()

    def get_groups(self):
        return [{'gp': member.gp, 'new_note': member.gp.new_note(self)}
                for member in group.models.GroupMember.objects.filter(member=self.id)]

    def get_picture(self):
        if self.profile_picture and self.profile_picture.url:
            return self.profile_picture.url
        return '/media/default-profile.jpeg'

    # returns all users which we owe to
    def get_owes_and_credits(self):
        all = dict()
        allInvolved = dng.models.Record.objects.filter(Q(debtor=self) | Q(creditor=self), is_paid=False)
        for rec in allInvolved:
            if rec.debtor == self:
                creditor = rec.creditor
                if creditor not in all:
                    all[creditor] = -rec.amount
                else:
                    all[creditor] -= rec.amount
            else:
                debtor = rec.debtor
                if debtor not in all:
                    all[debtor] = rec.amount
                else:
                    all[debtor] += rec.amount
        return all

    def get_owes(self):
        all = self.get_owes_and_credits()
        owes = dict()
        for it in all.items():
            if it[1] < 0:
                owes[it[0]] = -it[1]
        return owes

    # returns all users which owes user
    def get_credits(self):
        all = self.get_owes_and_credits()
        credits = dict()
        for it in all.items():
            if it[1] > 0:
                credits[it[0]] = it[1]
        return credits

    def get_owes_sum(self):
        owes = self.get_owes()
        return sum(owes.values())

    def get_credits_sum(self):
        credits = self.get_credits()
        return sum(credits.values())

    def get_total_balance(self):
        return self.get_credits_sum() - self.get_owes_sum()

    def get_notification(self):
        return [{
            'dong': g_notification.dong,
            'counting': g_notification.dong.counting(self) if g_notification.dong else 0,
            'notification': g_notification
        } for g_notification in group.models.Notification.objects.filter(person=self).order_by('-creation_date')]

    def get_notification_count(self):
        return group.models.Notification.objects.filter(person=self).filter(seen=False).count()


class Relation(models.Model):
    friend1 = models.ForeignKey(User, on_delete=models.CASCADE, related_name='friends')
    friend2 = models.ForeignKey(User, on_delete=models.CASCADE)
